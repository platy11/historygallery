var app = {}
app.currentState = 'list'

app.stateUpdate = function() {
  hash = window.location.hash.replace('#', '')
  if (hash === '' && app.currentState !== 'list') {
    document.querySelector('.sourcedisplay').style.display = 'none'
    document.querySelector('.cards').style.display = 'block'
    app.currentState = 'list'
  } else if (hash !== '') {
    app.updateDisplaySource(parseInt(hash))
    app.currentState = 'source'
  }
}

app.updateDisplaySource = function(sourceToDisplay) {
  //if (!sourceToDisplay) return
  document.querySelector('.loading').classList.add('active')
  setTimeout(() => {
    document.querySelector('.cards').style.display = 'none'
    document.querySelector('.sourcedisplay').style.display = 'block'
    let script = document.createElement('script')
    script.src = `data/${sourceToDisplay}.js`
    document.head.appendChild(script)
    document.querySelector('.sourcedisplay > h2').classList.remove('slide-now')
  }, 280)
}

app.goToState = function(index) {
  if (index === -1) {
    window.history.back()
  } else {
    history.pushState({}, 'Connections Virtual Gallery', '#' + index)
  }
  app.stateUpdate()
}

app.dataResponse = function(data) {
  document.querySelector('.sourcedisplay > h3').innerText = {
    pre: 'Pre-contact',
    at: 'At the time of contact',
    post: 'Post-contact',
    meta: 'Information'
  }[data.type]
  document.querySelector('.sourcedisplay > h3').setAttribute('class', data.type + '-el')
  document.querySelector('.sourcedisplay > h2').setAttribute('class', data.type + '-el')
  document.querySelector('.sourcedisplay > h2').innerText = data.title
  document.querySelector('.sourcetext').innerHTML = data.source.text
  document.querySelector('.sourceimage img').src = data.source.image.file
  document.querySelector('.sourceimage img').height = data.source.image.height
  for (let i = 0; i < data.content.length; i++) {
    document.querySelector('.source-' + i).innerHTML = data.content[i]
  }
  if (data.type === 'pre') {
    document.querySelector('.notpre').style.display = 'none'
  } else if (data.type === 'meta'){
    document.querySelector('.source').style.display = 'none'
    document.querySelectorAll('.notmeta').forEach(el => {
      el.style.display = 'none'
    })
  } else {
    document.querySelector('.source').style.display = 'block'
    document.querySelector('.notpre').style.display = 'block'
    document.querySelectorAll('.notmeta').forEach(el => {
      el.style.display = 'block'
    })
  }
  document.querySelector('.sourcedisplay > h2').classList.add('slide-now')
  document.querySelector('.loading').classList.remove('active')
}

document.addEventListener('DOMContentLoaded', () => {
  app.stateUpdate()
  window.addEventListener('popstate', app.stateUpdate)
  document.querySelectorAll('.card').forEach(el => {
    el.addEventListener('click', ev => app.goToState(ev.target.getAttribute('data-state-link')))
  })
})
