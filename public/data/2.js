app.dataResponse({
  type: 'at',
  title: 'A True Account of the Province of Cuzco (1547), Fransisco Xeres',
  source: {
    text: `Next morning the Spaniards marched in de-
    tachments in search of their enemies, and they again re-
    ceived punishment. When the captain saw that the harm
    they had received was sufficient, he sent messengers to pro-
    pose terms of peace to the chief. The chief of the province,
    which is called Quillimasa, sent one of his principal men
    back with the messengers, who made this reply that " by
    reason of the great fear he had of the Spaniards the chief
    had not come himself, but that if he was assured that he
    would not be killed, he would come peacefully." The cap-
    tain answered the messenger that "he would not be received
    badly, nor would he be injured, that he might come without
    fear as the Governor would receive him as a vassal of his
    Majesty, and would pardon the fault he had committed."
    With this assurance, though in great terror, the chief, with
    some principal men, came, and the captain received them
    joyfully, saying that " no harm would be done to those who
    came peacefully, though they had been in rebellion ; that as
    he had come, no more war would be made ; and that the
    people might return to their villages." Afterwards he or-
    dered the supplies he had found to be taken across the
    river, and he, with his Spaniards, returned to the place
    where he had left the Governor, taking the chief and the
    principal Indians with him. He reported what had taken
    place to the Governor, who gave thanks to our Lord for
    having granted a victory, without any Christian being
    wounded. He then told them to seek rest.<br>

    The Governor, seeing that the banks of that river were
    fertile and well peopled, ordered search to be made in the
    neighbourhood for a well sheltered port, and a very good one
    was found on the sea-coast, near the valley ; and Caciques
    or Lords of many vassals were found, in positions which
    were convenient for them to come and do service on the
    banks of the river. The Governor went to visit all these
    villages, and the district appeared to him to be suitable for
    a Spanish settlement. In order to comply with the com-
    mands of his Majesty, and that the natives might come to
    be converted and to receive a knowledge of our Holy
    Catholic Faith, he sent a messenger to the Spaniards who
    had been left at Tumbez, ordering them to come that, with
    the consent of all, a settlement might be formed on a site
    most convenient for his Majesty's service, and for the good
    of the natives. After he had sent this messenger, it oc-
    curred to him that delay might arise unless a person went,
    of whom the Caciques and Indians of Tumbez were in awe,
    so that they might assist in the march of the Spaniards.
    So he sent his brother Hernando Pizarro, Captain General.
    Afterwards the Governor learnt that certain chiefs in the
    hills would not submit, although they had received the
    orders of his Majesty, so he sent a Captain with twenty-five
    horse and foot, to reduce them to the service of his Majesty.
    Finding that they had abandoned their villages, the Captain
    sent to require them to come in peacefully, but they came
    prepared for war, and the Captain came out against them.
    In a short time many were killed and wounded, and they
    were defeated. The Captain once more demanded that they
    should submit, threatening that, if they refused, he would
    destroy them. So they submitted, and the Captain received
    them, and leaving all that province at peace, he returned to
    the place where the Governor remained bringing the chiefs
    with him. The Governor received them very kindly, and
    ordered them to return to their villages, and to bring back
    their people. The Captain said that he had found mines
    of fine gold in the hills round the villages of these chiefs,
    and that the inhabitants collected it. He brought speci-
    mens, and added that the mines were twenty leagues from
    that town.<br><br>
<em>From <a href="https://archive.org/stream/reportsondiscove04mark#page/n26/mode/1up">https://archive.org/stream/reportsondiscove04mark#page/n26/mode/1up</a></em>`,
    image: {
      file: 'data/accountcuzco.jpg',
      height: '350'
    }
  },
  content: [
    'This recount is written from the Spanish perspective of the first contact between Spanish adventurers and Incas around Tumbez, which is located in what is now north-west Peru. It details how the Spanish had a superior force which could make the Incas submit to the Spanish captain and governer.',
    'This source shows that the Incas were unsure of what the Spanish wanted, but recognised that they had superior European weapons which they could not fight. Because of this, the Incas were left with no choice but to surrender to the Spanish intruders. It suggests that the Spanish were trying to convert the Incas to their religion, which the Incas did not know at the time, but this would have a major impact on their civilisation into the future. From this text we can learn that Inca territory was divided in to smaller segements, provinces, and each one had its own chief who was responsible for keeping that province safe and managing it from day to day.',
    'One immediate negative impact from the arrival of the Spanish was the disruption to the Incan political/management system. As soon as the Spanish arrived, they were becoming more powerful as more chiefs made peace with them. This led to trouble for Incas as many of them were now under the control of people who had never seen them before, and had no idea of their customs and traditions. A positive impact was that Incas were now connected to other societies around the world, who they had the potential to trade with to obtain new resources. Spanish influence could also bring new technologies that were being developed in Europe around this time to bring increased effiecency in agriculture and other industries.', // Positive and negative
    'This source is reliable because it contains the Spanish perspective of the land they had just found, and their communications with the people that they found there. It was written by a person who was at the scene recording events which means it is unlikely to have major errors.' // Reliability and usefulness
  ]
})
