app.dataResponse({
  type: 'post',
  title: 'Virgin of Bethlehem (1740-1770)',
  source: {
    text: ``,
    image: {
      file: 'data/painting.jpg',
      height: '500'
    }
  },
  content: [
    'Entitled \'Virgin of Bethlehem\', this painting was made in the 1700s between 1740 and 1770 and the artist is unknown. The painting depicts Mary holding her son Jesus, both of them wearing decorative clothing in an elaborate room with other decorations around. Mary is the patron saint of Cusco, the former capital of the Incan Empire',
    'This shows that by this time the former Incas had started to become more religous, and people wanted to practise their religion in their art. The society had progressed further from its roots with the Incas and become more like a European country, adopting some Spanish and European customs. The painting shows that the society wanted to be different to what it used to be, it wanted to be closer to the other countries they knew at that stage, mainly European countries. This led to the reduction of use and commonality of culture coming from Incan times.',
    'The Spanish felt they were doing the right thing to bring their religion to these new lands, and it was adopted by many people from formerly Incan families. While this may have been a good thing, depending on your perspective, it led to Inca culture becoming more like an old tradition and less like a way to live for people descended from those families in that area. This shift in religion caused many changes in other aspects of culture, such as in the arts. Where Incan art had formerly been mostly geometric, and not depicting any people or objects, paintings and other forms of art began to be created that didn\'t follow this principle, especially with religious figures, such as in this painting.', // Positive and negative
    'The painting can be considered a reliable source because it has been confirmed by experts to come from the estimated time period and it clearly depicts someone\'s opinions from this time period. The usefulness is high because it can be easily compared to artworkds from earlier times in Incan culture and major differences can be spotted, such as what is depicted in the works.' // Reliability and usefulness
  ]
})
