app.dataResponse({
  type: 'pre',
  title: 'Incan textiles',
  source: {
    text: '',
    image: {
      file: 'data/textiles.jpg',
      height: '500'
    }
  },
  content: [
    'This is an Incan tunic made between 1450 and 1540 AD. It is coloured with warm tones and geometric patterns; there are no people, animals or natural features shown. It is possible that this was worn by a ruler, noble or someone who was important to the society. ',
    'This source tells us that the Incas had developed a cultural appreciation of arts and that they had equipment and knowledge to create a work like this. They would have developed their skills in weaving over time and they would have developed better equipment to create more advanced motifs and patterns. The textile also suggests that the Incas would have had a class/ruler system and that this tunic was a gift for someone more important than those who made it. The patterns also could have had some meaning for them, representing cultural beliefs or traditions they had. It is quite likely to be important for their culture in some way due to the care they have used to produce this tunic. After the Spanish arrived, art made by the Incas changed considerably and started to depict more people, particularly people related to Christianity which the Spaniards introduced to the Incas after their arrival.',
    '', // skip for pre
    'This source is reliable becuase it is a physical object created by the Incas, so it can not be influenced by personal views which may affect, for example, a record kept by a Spanish explorer watching the Incas. It is useful because they had no written language before contact with Europeans, so art is one of few types or record created by the Incas themselves. It is something we can directly compare between the two time periods, and it is also an area that had a lot of influence from Europeans after they arrived.'
  ]
})
