app.dataResponse({
  type: 'meta',
  title: 'Bibliography',
  source: {
    text: '',
    image: {
      file: '',
      height: 0
    }
  },
  content: [
    `<strong>Pre-contact</strong><br>Inca textile:<br>
     All T'oqapu Tunic - Dumbarton Oaks Research Library and Collection<br>
<a href="http://museum.doaks.org/Obj23071?sid=797&x=5448&sort=76">http://museum.doaks.org/Obj23071?sid=797&x=5448&sort=76</a><br><br>

Chronicles of the Incas:<br>
Pedro de Cieza de Léon: Chronicles of the Incas, 1540 - Modern History Sourcebook, Fordham University<br>
<a href="https://sourcebooks.fordham.edu/mod/1540cieza.asp">https://sourcebooks.fordham.edu/mod/1540cieza.asp</a><br><br>

<strong>At the time of contact</strong><br>
A True Account of the Provice of Cuzco - Archive.org<br>
<a href="https://archive.org/stream/reportsondiscove04mark#page/n26/mode/1up">https://archive.org/stream/reportsondiscove04mark#page/n26/mode/1up</a><br>
A True Account of the Provice of Cuzco - Archive.org [plain text version]<br>
<a href="https://ia600203.us.archive.org/28/items/reportsondiscove04mark/reportsondiscove04mark_djvu.txt">https://ia600203.us.archive.org/28/items/reportsondiscove04mark/reportsondiscove04mark_djvu.txt</a><br><br>

<strong>Post-contact</strong><br>Virgin of Bethlehem - Wikimedia Commons<br>
<a href="https://commons.wikimedia.org/wiki/File:Cuzque%C3%B1a2.jpg">https://commons.wikimedia.org/wiki/File:Cuzque%C3%B1a2.jpg</a><br>
Vigin of Bethlehem - Google Arts & Culture<br>
<a href="https://artsandculture.google.com/asset/JgGRr7Om3LRyEA?hl=pt-br#details">https://artsandculture.google.com/asset/JgGRr7Om3LRyEA?hl=pt-br#details</a><br><br>

Colonial Lima:<br>
Colonial Lima according to Jorge Juan and Antonio de Ulloa - historicaltextarchive.com<br>
<a href="http://historicaltextarchive.com/sections.php?action=read&artid=113">http://historicaltextarchive.com/sections.php?action=read&artid=113</a><br>
accessed via the Wayback Machine
<a href="https://web.archive.org/web/20170211141157/http://historicaltextarchive.com/sections.php?action=read&artid=113">https://web.archive.org/web/20170211141157/http://historicaltextarchive.com/sections.php?action=read&artid=113</a><br><br>
`,
    '',
    '',
    '.'
  ]
})
